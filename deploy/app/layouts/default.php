<!doctype html>
<html<?php $this->getHtmlAttribute(); ?> class="<?php $this->getHtmlClass(); ?> no-js" lang="pt-br">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?php echo $this->getTitle(); ?></title>
	<meta name="description" content="<?php echo $this->getDescription(); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="<?php echo $this->_asset('default/styles/main.css'); ?>">
	<link rel="stylesheet" href="<?php echo $this->_asset('bootstrap/css/bootstrap.min.css'); ?>">
	<script src="<?php echo $this->_asset('default/scripts/init.js'); ?>"></script>
	<!--[if lt IE 9]><script src="http://cdnjs.cloudflare.com/ajax/libs/es5-shim/2.0.8/es5-shim.min.js"></script><![endif]-->
	<?php $this->getHeadAppend(); ?>
</head>
<body<?php $this->getBodyAttribute(); ?> class="<?php $this->getBodyClass(); ?>">
	<?php $this->getAnalytics(); ?>
	<?php $this->getBodyPrepend(); ?>

	<header class="global-header" data-toggle-roll="40|sticky">
		<div class="global-header-container">
			<div class="clearfix">
				<h1><a href="#">Portal do Ingresso</a></h1>

				<a href="#" class="navsearch"></a>
				<a href="#" class="navicon"></a>

				<nav>
					<ul>
						<li><a href="#">Início</a></li>
						<li><a href="#">Crie seu evento, é grátis!</a></li>
						<li><a href="#">Imprimir ingresso</a></li>
						<li><a href="#">Criar conta</a></li>
						<!-- <li><a href="#">Login</a></li> -->
						<li class="session"><a href="#" class="button button-custom-1 button-dropdown"><span class="icon-person"></span> <strong>Fulano</strong></a>
							<ul class="dropdown">
								<li><a href="#">Painel de controle</a></li>
								<li><a href="#">Meus pedidos</a></li>
								<li class="divider"></li>
								<li><a href="#">Editar conta</a></li>
								<li><a href="#">Alterar senha</a></li>
								<li class="divider"></li>
								<li><a href="#">Sair</a></li>
							</ul>
						</li>
					</ul>
				</nav>

				<div class="form-search">
					<fieldset>
						<legend>Pesquisa</legend>

						<select name="cidade">
							<option value="">CIDADE *</option>
							<optgroup label="São Paulo">
								<option value="ATIBAIA">ATIBAIA</option>
								<option value="ITU">ITU</option>
								<option value="JUNDIAI">JUNDIAI</option>
								<option value="LORENA">LORENA</option>
								<option value="PINDAMONHANGABA">PINDAMONHANGABA</option>
								<option value="PIRACICABA">PIRACICABA</option>
								<option value="SAO JOSE DOS CAMPOS">SAO JOSE DOS CAMPOS DOS GOYTACAZES</option>
								<option value="SAO PAULO">SAO PAULO</option>
								<option value="SAO SEBASTIAO">SAO SEBASTIAO</option>
								<option value="SUMARE">SUMARE</option>
							</optgroup>
							<optgroup label="Minas Gerais">
								<option value="RIO PARANAIBA">RIO PARANAIBA</option>
							</optgroup>
							<optgroup label="Mato Grosso">
								<option value="SORRISO">SORRISO</option>
							</optgroup>
							<optgroup label="Rio de Janeiro">
								<option value="VOLTA REDONDA">VOLTA REDONDA</option>
							</optgroup>
						</select>

						<select name="categoria" required>
							<option value="">CATEGORIA *</option>
							<optgroup label="Group 1">
								<option value="1">Option 1</option>
								<option value="2">Option 2</option>
							</optgroup>
							<optgroup label="Group 2">
								<option value="3">Option 3</option>
								<option value="4">Option 4</option>
							</optgroup>
						</select>

						<select name="eventos" required>
							<option value="">EVENTOS *</option>
							<optgroup label="Group 1">
								<option value="1">Option 1</option>
								<option value="2">Option 2</option>
							</optgroup>
							<optgroup label="Group 2">
								<option value="3">Option 3</option>
								<option value="4">Option 4</option>
							</optgroup>
						</select>

						<input name="termo" type="text" placeholder="BUSQUE UM EVENTO" required>
						<button type="submit"></button>
					</fieldset>
				</div>
			</div>
		</div>
	</header>

	<div class="global-content">
		<?php $this->getView(); ?>
	</div>

	<footer class="global-footer">
		<div class="grid grid-items-5">
			<div class="grid-item">
				<h4 class="heading-topic">Como funciona</h4>
				<ul class="list-short">
					<li><a href="#">Venda seus ingressos online</a></li>
					<li><a href="#">Promova seu evento</a></li>
					<li><a href="#">Administre seu evento</a></li>
					<li><a href="#">Preço</a></li>
				</ul>
			</div>
			<div class="grid-item">
				<h4 class="heading-topic">Planeje seu evento</h4>
				<ul class="list-short">
					<li><a href="#">Congresso e seminário</a></li>
					<li><a href="#">Show, música e festa</a></li>
					<li><a href="#">Esportivo</a></li>
					<li><a href="#">Curso e workshop</a></li>
					<li><a href="#">Encontro e networking</a></li>
				</ul>
			</div>
			<div class="grid-item">
				<h4 class="heading-topic">Encontre eventos</h4>
				<ul class="list-short">
					<li><a href="#">São Paulo</a></li>
					<li><a href="#">Minas Gerais</a></li>
					<li><a href="#">Rio de Janeiro</a></li>
					<li><a href="#">Rio Grande do Sul</a></li>
					<li><a href="#">Outros estados</a></li>
				</ul>
			</div>
			<div class="grid-item">
				<h4 class="heading-topic">A empresa</h4>
				<ul class="list-short">
					<li><a href="#">Home</a></li>
					<li><a href="#">Sobre o portal</a></li>
					<li><a href="#">Trabalhe conosco</a></li>
					<li><a href="#">Contato</a></li>
					<li><a href="#">F.A.Q.</a></li>
				</ul>
			</div>
			<div class="grid-item">
				Facebook plugin here
			</div>
		</div>

		<div class="signature">
			<img src="<?php echo $this->_asset('default/images/signature-payment.png'); ?>" alt="" class="payment">

			<ul class="brands">
				<li><a href="#" target="_blank"><img src="<?php echo $this->_asset('default/images/logo-mjlema.png'); ?>" alt=""></a></li>
				<li><a href="#" target="_blank"><img src="<?php echo $this->_asset('default/images/logo-siteblindado.png'); ?>" alt=""></a></li>
				<li><a href="#" target="_blank"><img src="<?php echo $this->_asset('default/images/logo-antispambr.png'); ?>" alt=""></a></li>
				<li><a href="#" target="_blank"><img src="<?php echo $this->_asset('default/images/logo-comodo.png'); ?>" alt=""></a></li>
			</ul>
		</div>
	</footer>

	<script src="<?php echo $this->_asset('default/scripts/main.js'); ?>"></script>
	<script src="<?php echo $this->_asset('bootstrap/js/bootstrap.min.js'); ?>"></script>
	<?php $this->getBodyAppend(); ?>
</body>
</html>