<?php
class Cart extends Page
{
	public function __construct() {
		$this->setLayout('default');
		$this->setView('cart');
		$this->setTitle('Carrinho - Portal do Ingresso');
		$this->setDescription('');
		$this->setAnalytics(true);
	}
}
