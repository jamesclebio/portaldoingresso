<?php
class AuthenticationHelper
{
	public function sessionValidate() {
		if ($this->getName() !== 'area') {
			$_SESSION['location'] = $_SERVER['REQUEST_URI'];

			if (!$_SESSION['area_id']) {
				header('Location: ' . $this->_url('area'));
				exit;
			}
		}
	}

	public function setArea() {
		if (array_key_exists($this->_get('id'), AreaModel::getInstance()->getAreaList())) {
			$_SESSION['area_id'] = $this->_get('id');

			if ($_SESSION['location']) {
				header('Location: ' . $_SESSION['location']);
			} else {
				header('Location: ' . $this->_url('root'));
			}

			exit;
		}
	}

	public function getAreaId() {
		return $_SESSION['area_id'];
	}

	public function getAreaName() {
		$area_id = $_SESSION['area_id'];
		$area_list = AreaModel::getInstance()->getAreaList();

		return $area_list[$area_id];
	}
}
