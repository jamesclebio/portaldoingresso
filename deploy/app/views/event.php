<section>
	<header>
		<h2 class="heading-panel">Ingressos disponíveis</h2>
	</header>
	<div class="block-pane-bordered">
		<div class="alert alert-info alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			<p><strong>Atenção!</strong> Você deve fazer o login para selecionar seus ingressos. Se você ainda não for cadastrado, cadastre-se para comprar.</p>
		</div>

		<div class="table-responsive">
			<table class="table">
				<thead>
					<tr>
						<th>Ingresso</th>
						<th>Lote</th>
						<th>Sexo</th>
						<th>Tipo</th>
						<th class="align-right">Valor</th>
						<th class="align-center width-90">Quantidade</th>
						<th class="align-right">Subtotal</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><strong>Lorem ipsum dolor.</strong></td>
						<td>03</td>
						<td>Unissex</td>
						<td>Inteira</td>
						<td class="align-right"><strong>R$ 80,00</strong></td>
						<td class="align-center">
							<select>
								<option value="" disabled selected>Escolha</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
							</select>
						</td>
						<td class="align-right"><strong>R$ 80,00</strong></td>
					</tr>
					<tr>
						<td><strong>Lorem ipsum dolor.</strong></td>
						<td>03</td>
						<td>Unissex</td>
						<td>Inteira</td>
						<td class="align-right"><strong>R$ 80,00</strong></td>
						<td class="align-center">
							<select>
								<option value="" disabled selected>Escolha</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
							</select>
						</td>
						<td class="align-right"><strong>R$ 80,00</strong></td>
					</tr>
					<tr>
						<td><strong>Lorem ipsum dolor.</strong></td>
						<td>03</td>
						<td>Unissex</td>
						<td>Inteira</td>
						<td class="align-right"><strong>R$ 80,00</strong></td>
						<td class="align-center">
							<select>
								<option value="" disabled selected>Escolha</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
							</select>
						</td>
						<td class="align-right"><strong>R$ 80,00</strong></td>
					</tr>
				</tbody>
			</table>
		</div>

		<div class="grid grid-items-5">
			<div class="grid-item grid-item-span-3">
				<a href="#" class="button">Escolher seus ingressos</a>
			</div>
			<div class="grid-item grid-item-span-2">
				<div class="responsive-table">
					<table class="table table-bordered">
						<tr>
							<th>Subtotal:</th>
							<th>R$ 80,00</th>
						</tr>
						<tr>
							<th>Taxa de serviço:</th>
							<th>R$ 80,00</th>
						</tr>
						<tr class="highlight">
							<th>Total:</th>
							<th><strong>R$ 80,00</strong></th>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>

<div class="margin-top-30">
	<div class="grid grid-items-5">
		<div class="grid-item grid-item-span-2">
			<section>
				<header>
					<h2 class="heading-panel">Show de Banda eva</h2>
				</header>
				<div class="block-pane-bordered">
					<img src="<?php echo $this->_asset('default/images/event-detail-sample.jpg'); ?>" alt="" class="block-responsive">
				</div>
			</section>

			<div class="margin-top-30 tab">
				<ul class="tab-index">
					<li class="active"><a href="#tab-video"><span class="icon-ios7-videocam"></span> Vídeo do artista</a></li>
					<li><a href="#tab-mapa"><span class="icon-ios7-location"></span> Mapa do local</a></li>
				</ul>
				<div class="tab-content">
					<div id="tab-video">
						<div class="block-embed">
							<iframe src="http://www.youtube.com/embed/28h3SmnJZZI" frameborder="0" width="560" height="315"></iframe>
						</div>
					</div>

					<div id="tab-mapa">
						<div class="block-embed">
							 <iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.ch/maps?f=q&amp;source=s_q&amp;hl=de&amp;geocode=&amp;q=Bern&amp;aq=&amp;sll=46.813187,8.22421&amp;sspn=3.379772,8.453979&amp;ie=UTF8&amp;hq=&amp;hnear=Bern&amp;t=m&amp;z=12&amp;ll=46.947922,7.444608&amp;output=embed&amp;iwloc=near"></iframe>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="grid-item grid-item-span-3">
			<section>
				<header>
					<h2 class="heading-panel">Dia 23/05/2014, a partir das 23h</h2>
				</header>
				<div class="block-pane-bordered">
					<div class="block-pane-invert block-notice">
						<p><strong>Cidade:</strong> Natal-RN</p>
						<p><strong>Local:</strong> Pink Elephant Natal</p>
					</div>

					<div class="margin-top-30">
						<iframe src="//www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Ffreedomdigital&amp;width&amp;layout=button_count&amp;action=like&amp;show_faces=false&amp;share=true&amp;height=21&amp;appId=208233275897800" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:21px;" allowTransparency="true"></iframe>
					</div>

					<div class="margin-top-30 text">
						<h1>Show de Banda Eva</h1>

						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem debitis magnam excepturi dolor quis assumenda, libero fuga at a provident.</p>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum iste hic, alias voluptas ullam modi quos dolor, pariatur aspernatur tempore nam neque. Fuga neque ipsa, voluptate cumque, nisi excepturi commodi.</p>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum fugit voluptatum doloremque corrupti. Expedita optio vel, sed doloribus quisquam, fuga accusamus, ab laudantium quo nemo aperiam et cumque architecto odio.</p>
					</div>

					<div class="alert alert-info alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
						<p><strong>Atenção!</strong> A qualidade e/ou efetivo cumprimento deste evento é de responsabilidade exclusiva do organizador, bem como todas as informações, preços e políticas promocionais aqui divulgadas.</p>
						<p>Qualquer dúvida leia a F.A.Q.</p>
					</div>

					<div class="separate block-notice">
						<p>Avaliações: <strong>Sem avaliações</strong></p>
						<p>Avalie este evento:</p>
						<ul class="list-rating">
							<li class="on"><a href="#"></a></li>
							<li class="on"><a href="#"></a></li>
							<li><a href="#"></a></li>
							<li><a href="#"></a></li>
							<li><a href="#"></a></li>
						</ul>
					</div>

				</div>
			</section>
		</div>
	</div>
</div>

<section class="margin-top-30">
	<header>
		<h2 class="heading-panel">Pontos de venda</h2>
	</header>
	<div class="block-pane-bordered">
		content
	</div>
</section>

<section class="margin-top-30">
	<header>
		<h2 class="heading-panel">Outros eventos da categoria</h2>
	</header>
	<div class="block-pane-bordered">
		content
	</div>
</section>

