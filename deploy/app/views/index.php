<div class="slider">
	<div class="container">
		<div class="item"><a href="#1"><img src="<?php echo $this->_asset('default/images/slider/ousadia-alegria-1.jpg'); ?>" alt=""></a></div>
		<div class="item"><a href="#2"><img src="<?php echo $this->_asset('default/images/slider/ousadia-alegria-2.jpg'); ?>" alt=""></a></div>
		<div class="item"><a href="#3"><img src="<?php echo $this->_asset('default/images/slider/ousadia-alegria-3.jpg'); ?>" alt=""></a></div>
	</div>
</div>

<section>
	<header class="heading-section">
		<h2>Eventos em destaque</h2>
	</header>

	<div class="grid grid-rows grid-items-3">
		<div class="grid-item">
			<div class="block-event">
				<div class="image">
					<a href="#" class="image"><img src="<?php echo $this->_asset('default/images/event-list-sample.jpg'); ?>" alt=""></a>
				</div>
				<div class="text">
					<a href="#" class="title">Lorem ipsum dolor</a>
					<div class="clearfix">
						<div class="more">
							<div class="when">28 AGO a 02 SET</div>
							<a href="#">Saiba mais</a>
						</div>
						<div class="details">
							<div class="place">Clube X</div>
							<div class="time">A partir das 22h</div>
							<div class="locale">Aracaju-SE</div>
							<div class="price">R$50,00</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="grid-item">
			<div class="block-event">
				<div class="image">
					<a href="#" class="image"><img src="<?php echo $this->_asset('default/images/event-list-sample.jpg'); ?>" alt=""></a>
				</div>
				<div class="text">
					<a href="#" class="title">Lorem ipsum dolor sit amet, consectetur adipisicing</a>
					<div class="clearfix">
						<div class="more">
							<div class="when">28 AGO a 02 SET</div>
							<a href="#">Saiba mais</a>
						</div>
						<div class="details">
							<div class="place">Clube X</div>
							<div class="time">A partir das 22h</div>
							<div class="locale">Aracaju-SE</div>
							<div class="price">R$50,00</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="grid-item">
			<div class="block-event">
				<div class="image">
					<a href="#" class="image"><img src="<?php echo $this->_asset('default/images/event-list-sample.jpg'); ?>" alt=""></a>
				</div>
				<div class="text">
					<a href="#" class="title">Lorem ipsum dolor</a>
					<div class="clearfix">
						<div class="more">
							<div class="when">28 AGO a 02 SET</div>
							<a href="#">Saiba mais</a>
						</div>
						<div class="details">
							<div class="place">Clube X</div>
							<div class="time">A partir das 22h</div>
							<div class="locale">Aracaju-SE</div>
							<div class="price">R$50,00</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="grid-item">
			<div class="block-event">
				<div class="image">
					<a href="#" class="image"><img src="<?php echo $this->_asset('default/images/event-list-sample.jpg'); ?>" alt=""></a>
				</div>
				<div class="text">
					<a href="#" class="title">Lorem ipsum dolor</a>
					<div class="clearfix">
						<div class="more">
							<div class="when">28 AGO a 02 SET</div>
							<a href="#">Saiba mais</a>
						</div>
						<div class="details">
							<div class="place">Clube X</div>
							<div class="time">A partir das 22h</div>
							<div class="locale">Aracaju-SE</div>
							<div class="price">R$50,00</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="grid-item">
			<div class="block-event">
				<div class="image">
					<a href="#" class="image"><img src="<?php echo $this->_asset('default/images/event-list-sample.jpg'); ?>" alt=""></a>
				</div>
				<div class="text">
					<a href="#" class="title">Lorem ipsum dolor</a>
					<div class="clearfix">
						<div class="more">
							<div class="when">28 AGO a 02 SET</div>
							<a href="#">Saiba mais</a>
						</div>
						<div class="details">
							<div class="place">Clube X</div>
							<div class="time">A partir das 22h</div>
							<div class="locale">Aracaju-SE</div>
							<div class="price">R$50,00</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="grid-item">
			<div class="block-event">
				<div class="image">
					<a href="#" class="image"><img src="<?php echo $this->_asset('default/images/event-list-sample.jpg'); ?>" alt=""></a>
				</div>
				<div class="text">
					<a href="#" class="title">Lorem ipsum dolor</a>
					<div class="clearfix">
						<div class="more">
							<div class="when">28 AGO a 02 SET</div>
							<a href="#">Saiba mais</a>
						</div>
						<div class="details">
							<div class="place">Clube X</div>
							<div class="time">A partir das 22h</div>
							<div class="locale">Aracaju-SE</div>
							<div class="price">R$50,00</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<a href="#" class="button button-block">Listar mais eventos</a>
</section>

<section class="margin-top-30">
	<header class="heading-section">
		<h2>Organize o seu evento</h2>
		<h4>Saiba como organizar o seu evento passo a passo</h4>
	</header>

	<div class="grid grid-items-3">
		<div class="grid-item">
			<a href="#" class="block-step"><img src="<?php echo $this->_asset('default/images/block-step-1.png'); ?>" alt=""><span>Passo 1</span></a>
		</div>
		<div class="grid-item">
			<a href="#" class="block-step"><img src="<?php echo $this->_asset('default/images/block-step-2.png'); ?>" alt=""><span>Passo 2</span></a>
		</div>
		<div class="grid-item">
			<a href="#" class="block-step"><img src="<?php echo $this->_asset('default/images/block-step-3.png'); ?>" alt=""><span>Passo 3</span></a>
		</div>
	</div>
</section>
