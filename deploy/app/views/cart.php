<div class="alert alert-warning alert-dismissible" role="alert">
	<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
	<p><strong>Warning!</strong> Better check yourself, you're not looking too good.</p>
</div>

<form role="form">
	<section>
		<header>
			<h2 class="heading-panel">Carrinho de compras (ingressos)</h2>
		</header>

		<div class="block-pane-bordered">
			<div class="table-responsive">
				<table class="table">
					<thead>
						<tr>
							<th>Flyer</th>
							<th>Ingresso</th>
							<th class="align-right">Valor</th>
							<th class="align-center width-90">Quantidade</th>
							<th class="align-right">Taxa de serviço</th>
							<th class="align-right">Subtotal</th>
							<th class="align-center">Excluir</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><img src="<?php echo $this->_asset('default/images/event-cart-sample.jpg'); ?>" alt=""></td>
							<td>
								<strong>Show de Banda Eva</strong><br>
								Araras-SP<br>
								Clube X, a partir das 22h<br>
								Camarote Premium Open Bar, Lote 1<br>
								Unissex, Meia entrada
							</td>
							<td class="align-right"><strong>R$ 80,00</strong></td>
							<td class="align-center">
								<select name="" required>
									<option value="1" selected>1</option>
									<option value="2">2</option>
									<option value="3">3</option>
								</select>
							</td>
							<td class="align-right">R$ 80,00</td>
							<td class="align-right"><strong>R$ 80,00</strong></td>
							<td class="align-center"><a href="#" class="button button-small" title="Excluir"><span class="icon-trash-a"></span></a></td>
						</tr>
						<tr>
							<td><img src="<?php echo $this->_asset('default/images/event-cart-sample.jpg'); ?>" alt=""></td>
							<td>
								<strong>Show de Banda Eva</strong><br>
								Araras-SP<br>
								Clube X, a partir das 22h<br>
								Camarote Premium Open Bar, Lote 1<br>
								Unissex, Meia entrada
							</td>
							<td class="align-right"><strong>R$ 80,00</strong></td>
							<td class="align-center">
								<select name="" required>
									<option value="1" selected>1</option>
									<option value="2">2</option>
									<option value="3">3</option>
								</select>
							</td>
							<td class="align-right">R$ 80,00</td>
							<td class="align-right"><strong>R$ 80,00</strong></td>
							<td class="align-center"><a href="#" class="button button-small" title="Excluir"><span class="icon-trash-a"></span></a></td>
						</tr>
						<tr>
							<td><img src="<?php echo $this->_asset('default/images/event-cart-sample.jpg'); ?>" alt=""></td>
							<td>
								<strong>Show de Banda Eva</strong><br>
								Araras-SP<br>
								Clube X, a partir das 22h<br>
								Camarote Premium Open Bar, Lote 1<br>
								Unissex, Meia entrada
							</td>
							<td class="align-right"><strong>R$ 80,00</strong></td>
							<td class="align-center">
								<select name="" required>
									<option value="1" selected>1</option>
									<option value="2">2</option>
									<option value="3">3</option>
								</select>
							</td>
							<td class="align-right">R$ 80,00</td>
							<td class="align-right"><strong>R$ 80,00</strong></td>
							<td class="align-center"><a href="#" class="button button-small" title="Excluir"><span class="icon-trash-a"></span></a></td>
						</tr>
					</tbody>
				</table>
			</div>

			<div class="grid grid-items-5">
				<div class="grid-item grid-item-span-3">
					<a href="#" class="button">Continuar comprando</a>
				</div>
				<div class="grid-item grid-item-span-2">
					<div class="responsive-table">
						<table class="table table-bordered">
							<tr>
								<th>Subtotal:</th>
								<th>R$ 80,00</th>
							</tr>
							<tr>
								<th>Taxa de serviço:</th>
								<th>R$ 80,00</th>
							</tr>
							<tr class="highlight">
								<th>Total:</th>
								<th><strong>R$ 80,00</strong></th>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="margin-top-30">
		<header>
			<h2 class="heading-panel">Forma de recebimento dos ingressos</h2>
		</header>

		<div class="block-pane-bordered">
			<div class="block-device">
				<div class="alert alert-info alert-dismissible" role="alert">
					<div class="checkbox">
						<label><input name="" type="checkbox" value="1" required>Desejo receber um SMS de confirmação de recebimento com o link para visualização dos vale-tickets pelo celular.</label>
					</div>
				</div>

				<p>Este serviço envia um SMS para seu celular informando que os vale-tickets já estão disponíveis como também um link para visualização do mesmo pelo celular, com um custo adicional de R$ 0,40 que será adicionado no valor total de sua compra.</p>
			</div>
		</div>
	</section>

	<section class="margin-top-30">
		<header>
			<h2 class="heading-panel">Confirmação do número de seu celular</h2>
		</header>

		<div class="block-pane-bordered">
			<div class="input-group width-250 margin-bottom-10">
				<input type="text" class="form-control mask-phone">
				<span class="input-group-btn">
					<button class="btn btn-info" type="button">Corrigir</button>
				</span>
			</div>

			<p>A confirmação do seu número de celular só é necessária caso você opte por receber a confirmação de entrega dos vale-tickets via SMS, se o número mostrado ao lado estiver correto não é necessário confirmar!</p>
		</div>
	</section>

	<section class="margin-top-30">
		<header>
			<h2 class="heading-panel">Observações para o seu pedido</h2>
		</header>

		<div class="block-pane-bordered">
			<div class="form-group">
				<label for="">Mensagem</label>
				<textarea class="form-control" rows="3" maxlength="400" data-field-count></textarea>
			</div>
		</div>
	</section>

	<section class="margin-top-30">
		<header>
			<h2 class="heading-panel">Pagamento</h2>
		</header>

		<div class="block-pane-bordered">
			<div class="panel panel-info">
				<div class="panel-heading">
					<h3 class="panel-title">Políticas e condições de venda do Portal do Ingresso</h3>
				</div>

				<div class="panel-body">
					<div class="block-document">
						<h4>1. Lorem ipsum dolor sit amet</h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate, dolorem, placeat voluptate nemo eum sint ex aliquid reiciendis quaerat asperiores deleniti quasi id obcaecati laborum maiores vel est. Odit, tempora!</p>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur, neque illo sapiente dolore fugit enim in aspernatur veniam sequi laudantium?</p>

						<h4>2. Lorem ipsum dolor sit amet</h4>
						<ul>
							<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
							<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
							<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
							<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
							<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
						</ul>
					</div>
				</div>
			</div>

			<div class="checkbox">
				<label><input name="" type="checkbox" value="1" required>Li e aceito as políticas e condições de venda do site.</label>
			</div>
		</div>
	</section>

	<div class="block-action">
		<button type="submit" class="btn btn-lg btn-success">Efetuar pagamento</button>
	</div>
</form>

