module.exports = function(grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		project: {
			banner:	'/* Built on <%= grunt.template.today("dddd, mmmm dS, yyyy, h:MM:ss TT") %> */',
			default: {
				scripts: {
					main: [
						'bower_components/jquery/dist/jquery.js',
						'bower_components/jquery-maskedinput/dist/jquery.maskedinput.js',
						'bower_components/carouFredSel/jquery.carouFredSel-6.2.1.js',
						'bower_components/sifter/sifter.js',
						'bower_components/microplugin/src/microplugin.js',
						'bower_components/selectize/dist/js/selectize.js',
						'assets/default/scripts/main.core.js',
						'assets/default/scripts/main.go-roll.js',
						'assets/default/scripts/main.go-hash.js',
						'assets/default/scripts/main.go-top.js',
						'assets/default/scripts/main.toggle-roll.js',
						'assets/default/scripts/main.form.js',
						'assets/default/scripts/main.field-extend.js',
						'assets/default/scripts/main.field-count.js',
						'assets/default/scripts/main.dropdown.js',
						'assets/default/scripts/main.tab.js',
						'assets/default/scripts/main.slider.js',
						'assets/default/scripts/main.rating.js',
						'assets/default/scripts/main.navsearch.js',
						'assets/default/scripts/main.navicon.js',
						'assets/default/scripts/main.resize.js',
						'assets/default/scripts/main.init.js',
						'assets/default/scripts/main.js'
					],
					init: [
						'bower_components/modernizr/modernizr.js',
						'assets/default/scripts/init.js'
					]
				},
				styles: {
					path: 'assets/default/styles',
					compile: '<%= project.default.styles.path %>/**/*.sass'
				}
			}
		},

		jshint: {
			options: {
				force: true
			},
			default_main: ['assets/default/scripts/**/*', '!assets/default/scripts/init.js'],
			default_init: ['assets/default/scripts/init.js']
		},

		uglify: {
			options: {
				banner:	'<%= project.banner %>\n'
			},
			default_main: {
				src: '<%= project.default.scripts.main %>',
				dest: 'deploy/assets/default/scripts/main.js'
			},
			default_init: {
				src: '<%= project.default.scripts.init %>',
				dest: 'deploy/assets/default/scripts/init.js'
			}
		},

		compass: {
			options: {
				banner:	'<%= project.banner %>',
				relativeAssets: true,
				noLineComments: true,
				outputStyle: 'expanded',
				raw: 'preferred_syntax = :sass'
			},
			default: {
				options: {
					sassDir: '<%= project.default.styles.path %>',
					specify: '<%= project.default.styles.compile %>',
					cssDir: 'deploy/assets/default/styles',
					imagesDir: 'deploy/assets/default/images',
					javascriptsDir: 'deploy/assets/default/scripts',
					fontsDir: 'deploy/assets/default/fonts'
				}
			}
		},

		copy: {
			bootstrap: {
				expand: true,
				cwd: 'bower_components/bootstrap/dist/',
				src: '**',
				dest: 'deploy/assets/bootstrap/',
				filter: 'isFile'
			},
			deploy_html: {
				expand: true,
				cwd: 'deploy/assets/',
				src: '**',
				dest: 'deploy-html/assets/',
				filter: 'isFile'
			}
		},

		watch: {
			options: {
				livereload: true,
				spawn: false
			},
			grunt: {
				files: ['Gruntfile.js'],
				tasks: ['default']
			},
			default_scripts_main: {
				files: ['<%= project.default.scripts.main %>'],
				tasks: ['jshint:default_main', 'uglify:default_main']
			},
			default_scripts_init: {
				files: ['<%= project.default.scripts.init %>'],
				tasks: ['jshint:default_init', 'uglify:default_init']
			},
			default_styles: {
				files: ['<%= project.default.styles.compile %>'],
				tasks: ['compass:default']
			},
			app: {
				files: ['deploy/app/**/*']
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-compass');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-watch');

	grunt.registerTask('default', ['jshint', 'uglify', 'compass', 'copy:bootstrap']);
	grunt.registerTask('deploy_html', ['copy:deploy_html']);
};

