;(function($, window, document, undefined) {
	'use strict';

	main.init = function() {
		var	plugins = [
			'goHash',
			'goTop',
			'toggleRoll',
			'form',
			'fieldExtend',
			'fieldCount',
			'dropdown',
			'tab',
			'slider',
			'rating',
			'navsearch',
			'navicon',
			'resize'
		];

		if (arguments.length) {
			plugins = arguments;
		}

		for (var i in plugins) {
			this[plugins[i]].init();
		}
	};

	main.init();
}(jQuery, this, this.document));
