;(function($, window, document, undefined) {
	'use strict';

	main.tab = {
		wrapper: '.tab',

		build: function() {
			var	that = this,
				$wrapper = $(that.wrapper);			

			$wrapper.find('.tab-index .active').each(function() {
				that.toggle($(this).find('a'));
			});
		},

		toggle: function($this) {
			var	that = this,
				$tab_index = $this.closest(that.wrapper).find('.tab-index'),
				$tab_content = $this.closest(that.wrapper).find('.tab-content'),
				class_active = 'active';

			$tab_index.find('li').removeClass(class_active);
			$tab_content.find('> div').hide();
			$this.closest('li').addClass(class_active);
			$($this.attr('href')).show();
		},

		bindings: function() {
			var	that = this,
				$wrapper = $(that.wrapper);

			$wrapper.find('.tab-index a').on({
				click: function(e) {
					that.toggle($(this));
					e.preventDefault();
				}
			});
		},

		init: function() {
			var	$wrapper = $(this.wrapper);

			if ($wrapper.length) {
				this.build();
				this.bindings();
			}
		}
	};
}(jQuery, this, this.document));
