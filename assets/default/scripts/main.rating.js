;(function($, window, document, undefined) {
	'use strict';

	main.rating = {
		wrapper: '.list-rating',

		bindings: function() {
			var	that = this,
				$wrapper = $(that.wrapper),
				class_on = 'on-hover',
				class_off = 'off-hover';

			$wrapper.find('a').on({
				mouseenter: function() {
					var	$on = $(this).closest('li').prevAll('li'),
						$off = $(this).closest('li').nextAll('li');

					$on.addClass(class_on);
					$off.addClass(class_off);
				},

				mouseleave: function() {
					var	$on = $(this).closest('li').prevAll('li'),
						$off = $(this).closest('li').nextAll('li');

					$on.removeClass(class_on);
					$off.removeClass(class_off);
				}
			});
		},

		init: function() {
			var	$wrapper = $(this.wrapper);

			if ($wrapper.length) {
				this.bindings();
			}
		}
	};
}(jQuery, this, this.document));
