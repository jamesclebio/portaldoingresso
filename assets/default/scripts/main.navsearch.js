;(function($, window, document, undefined) {
	'use strict';

	main.navsearch = {
		wrapper: '.navsearch',

		bindings: function() {
			var	that = this,
				$wrapper = $(that.wrapper),
				$nav = $('.global-header .form-search');

			$wrapper.on({
				click: function(e) {
					$nav.slideToggle();
					e.preventDefault();
				}
			});
		},

		init: function() {
			var	that = this,
				$wrapper = $(that.wrapper);

			if ($wrapper.length) {
				that.bindings();
			}
		}
	};
}(jQuery, this, this.document));
