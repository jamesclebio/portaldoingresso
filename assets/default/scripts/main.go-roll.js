;(function($, window, document, undefined) {
	'use strict';

	main.goRoll = function(target, diff) {
		var	$parent = $('body, html'),
			$target = $(target);

		if (!$target.length) {
			$target = $parent;
		}

		$parent.animate({
			scrollTop: $target.offset().top + diff
		});
	};
}(jQuery, this, this.document));
