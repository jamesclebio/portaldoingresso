;(function($, window, document, undefined) {
	'use strict';

	main.navicon = {
		wrapper: '.navicon',

		bindings: function() {
			var	that = this,
				$wrapper = $(that.wrapper),
				$nav = $('.global-header nav');

			$wrapper.on({
				click: function(e) {
					$nav.slideToggle();
					e.preventDefault();
				}
			});
		},

		init: function() {
			var	that = this,
				$wrapper = $(that.wrapper);

			if ($wrapper.length) {
				that.bindings();
			}
		}
	};
}(jQuery, this, this.document));
