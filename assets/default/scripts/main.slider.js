;(function($, window, document, undefined) {
	'use strict';

	main.slider = {
		wrapper: '.slider',

		build: function() {
			var	that = this,
				$wrapper = $(that.wrapper),
				template_index = '<div class="index"></div>',
				template_nav = '<div class="nav"><a href="#" class="prev"></a><a href="#" class="next"></a></div>';

			$wrapper.append(template_index);
			$wrapper.append(template_nav);

			$wrapper.find('.container').carouFredSel({
				height: 'variable',
				items: {
					height: 'variable',
					visible: 1,
					minimum: 2
				},
				scroll: {
					fx: 'crossfade'
				},
				auto: {
					timeoutDuration: 4000
				},
				pagination: $wrapper.find('.index'),
				prev: $wrapper.find('.nav .prev'),
				next: $wrapper.find('.nav .next'),
				responsive: true
			});
		},

		init: function() {
			var	$wrapper = $(this.wrapper);

			if ($wrapper.length) {
				this.build();
			}
		}
	};
}(jQuery, this, this.document));

