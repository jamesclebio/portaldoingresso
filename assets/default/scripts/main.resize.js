;(function($, window, document, undefined) {
	'use strict';

	main.resize = {
		bindings: function() {
			var	that = this,
				$nav = $('.global-header nav'),
				$search = $('.global-header .form-search');

			$(window).resize(function() {
				if ($(window).width() > 820) {
					$search.removeAttr('style');
					$nav.removeAttr('style');
				}
			});
		},

		init: function() {
			var	that = this;

			that.bindings();
		}
	};
}(jQuery, this, this.document));
